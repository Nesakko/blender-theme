# Blender Breeze Dark theme :

This theme mimics the color scheme of the Breeze Dark KDE theme

## How to Install :

Download the `breeze_dark.xml`, or clone the repo...

In Blender, go to `user preferences` -> `Themes` -> `Install...` -> select `breeze_dark.xml` file :)

### For Arch power user:

An AUR package exists https://aur.archlinux.org/packages/blender-breezedark-theme-git

`yay -S blender-breezedark-theme-git`

(This package is not maintained by me)

---

- main repo : https://codeberg.org/Nesakko/blender-BreezeDark-theme
- mirror repo : https://github.com/Nesakko/blender-BreezeDark-theme
- mirror repo : https://gitlab.com/Nesakko/blender-theme

About issue :  
Going only very occasionally on GitHub, and _almost_ never on GitLab, I will turn off GH/GL issues, prefer forking it or create your issues on Codeberg :)

---

![](Images/Screenshot_20191016_212808.png)
![](Images/Screenshot_20191016_212845.png)
